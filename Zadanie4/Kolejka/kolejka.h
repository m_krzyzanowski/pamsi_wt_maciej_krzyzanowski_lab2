#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

template <typename T>
class element
{
public:
    T wartosc;
	element *nastepny;
	element()           //konstruktor elementu
	{
		wartosc = NULL;
		nastepny = NULL;
	}
};

template <typename T>
class Kolejka
{
private:
    element <T> *glowa;
    element <T> *ogon;
    int l_elementow;

public:

    Kolejka() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        l_elementow = 0;
    }

    ~Kolejka() //destruktor
    {
        element <T> *wsk;
        while(glowa != NULL)
        {
            wsk = glowa;
            glowa = glowa->nastepny;
            delete wsk;
        }
    }

    void wyswietl(); //wyswietla zawartosc kolejki
    void dodaj_element(); //dodaje element na koniec kolejki
    bool usun_element(); //usuwa element z poczatku kolejki (Zwraca TRUE jesli sie udalo, zwrata FALSE i wyswietla odpowiedni komunkat jesli sie nie udalo)
    void usun_wszystkie(); //usuwa wszystkie elementy kolejki
    void menu_kolejka(); //wyswietla menu kolejki
};


//---------------------------------metody-----------------------------------

template <typename T>
void Kolejka<T>::wyswietl()
{
    element <T> *wsk = glowa;
    cout << "KOLEJKA: " << endl;
    cout << "------------------------" << endl;
    while(wsk != NULL)
    {
       cout << wsk->wartosc << " ";
       wsk = wsk->nastepny;
    }
    cout << endl << "------------------------" << endl << endl;
}

template <typename T>
void Kolejka<T>::dodaj_element()
{
    T elem;
    cout << "PODAJ: ";
    while(!(cin >> elem))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
    {
        cerr << "(Niepoprawna wartosc.)" << endl;
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }
	element <T> *nowy = new element <T>;
    nowy->wartosc = elem;
    nowy->nastepny = NULL;
    if(l_elementow==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
		ogon->nastepny = nowy;
		ogon = nowy;
    }
    cin.sync();
    l_elementow++;
}

template <typename T>
bool Kolejka<T>::usun_element()
{
    element <T> *wsk = glowa;
    if(!(wsk))
    {
        cout << "Kolejka jest pusta.";
        Sleep(1000);
        return false;
    }
    glowa = glowa->nastepny;
    delete wsk;
    l_elementow--;
    return true;
}

template <typename T>
void Kolejka<T>::usun_wszystkie()
{
    element <T> *wsk;
    while(glowa != NULL)
    {
        wsk = glowa;
        glowa = glowa->nastepny;
        delete wsk;
    }
}

template <typename T>
void Kolejka<T>::menu_kolejka()
{
    char option;
        do{
            system("cls");
            Kolejka::wyswietl();
            cout << "==== MENU KOLEJKA ===" << endl;
            cout << "1) Dodawanie elementu do kolejki." << endl;
            cout << "2) Usuwanie pojedynczego elementu z kolejki." << endl;
            cout << "3) Usuwanie wszystkich elementow z kolejki." << endl;
            cout << "0) Wyjdz." << endl;
            cout << "===================" << endl;
            cout << "Podaj opcje: ";
            option = getche();
            cout << endl;

            switch(option)
            {
            case '1':
                Kolejka::dodaj_element();
                break;

            case '2':
                Kolejka::usun_element();
                break;

            case '3':
                Kolejka::usun_wszystkie();
                break;
            }

            } while (option != '0');

}
