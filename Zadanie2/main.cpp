#include <iostream>
#include <conio.h>
#include <windows.h>

using namespace std;


template <typename T>
struct element
{
    T wartosc; //wartosc elementu
    element *nastepny; //wskaznik na kolejny elemenet
    element()
    {
        wartosc = NULL;
        nastepny = NULL; //konstruktor wskaznika
    }
};

template <typename T>
struct Lista
{
    element <T> *glowa;  //wskaznik na pierwszy el. listy
    int l_elementow; //licznik el.
    void dodaj_poczatek(); //f. dodaje elem. na poczatek
    bool dodaj_wybrany(); //f. dodaje elem. w wybrane miejsce
    bool usun_wybrany(); //f. usuwa wybrany element
    void usun_wszystkie(); //f. usuwa wszystkie elem.
    void wyswietl();

    Lista()
    {
        glowa = NULL;
        l_elementow = 0;
    }

    ~Lista()
    {
        element <T> *wsk;
        while(glowa != NULL)
        {
            wsk = glowa;
            glowa = glowa->nastepny;
            delete wsk;
        }
        l_elementow = 0;
    }
};

template <typename T>
void Lista<T>::dodaj_poczatek()
{
    T elem;
    cout << "PODAJ: ";
    while(!(cin >> elem))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
    {
        cerr << "(Niepoprawna wartosc.)" << endl;
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }
	element <T> *nowy = new element <T>;
    nowy->wartosc = elem;
    nowy->nastepny = glowa;
    glowa = nowy;
    cin.sync();
    l_elementow++;
}

template <typename T>
bool Lista<T>::dodaj_wybrany()
{
    T elem;
    int wybor;
    cout << "Miejsce: ";
    while(!(cin >> wybor))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
    {
        cerr << "(Niepoprawna wartosc.)" << endl;
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }

    if(wybor > l_elementow+1)
    {
        cout << "(Brak takiego elementu)";
        Sleep(1000);
        return false;
    }

    cout << "PODAJ: ";
    cin >> elem;

	element <T> *nowy = new element <T>;

    //jezeli to pierwszy element
    if (wybor==1)
    {
        nowy->wartosc = elem;
        nowy->nastepny = glowa;
        glowa = nowy;
    }

    //jezeli to kolejny element
    if (wybor>=2)
    {
        element <T> *wsk = glowa;
        int i = 1;
        while(wsk)
        {
            if((i+1) == wybor) break; //przesuwanie zmiannej wskaznikowej wsk, do wezla poprzedzajacego usuwany element
            wsk = wsk->nastepny;
            i++;
        }
        if (wsk->nastepny == 0) //jezeli dodajemy na koniec listy
        {
            wsk->nastepny = nowy;
            nowy->wartosc = elem;
            nowy->nastepny = NULL;
        }
        else //jezeli inny
        {
            nowy->nastepny = wsk->nastepny;
            wsk->nastepny = nowy;
            nowy->wartosc = elem;
        }
    }
    l_elementow++;
    return true;
}

template <typename T>
bool Lista<T>::usun_wybrany()
{
    int wybor;
    cout << "Element: ";
    while(!(cin >> wybor))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
    {
        cerr << "(Niepoprawna wartosc.)" << endl;
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }

    if(wybor > l_elementow+1)
    {
        cout << "(Brak takiego elementu)";
        Sleep(1000);
        return false;
    }

    //jezeli usuwamy pierwszy kontener listy
    if (wybor==1)
    {
        element <T> *wsk = glowa;
        glowa = wsk->nastepny;
        delete wsk;
    }

    //jezeli usuwamy kolejne kontener
    if(wybor>=2)
    {
        element <T> *wsk = glowa; //ustawiamy wskaznik na pierwszy kontener
        int i = 1;
        //ustawianie wskaznika na kontener poprzedzajacy usuwany kontener
        while(wsk)
        {
            if((i+1) == wybor) break;
            wsk = wsk->nastepny;
            i++;
        }
        if (wsk->nastepny->nastepny==0) //sprawdzenie czy to nie jest ostani kontener
            wsk->nastepny = NULL;
        else
            wsk->nastepny = wsk->nastepny->nastepny;

        delete wsk->nastepny;
    }
    l_elementow--;
    return true;

}

template <typename T>
void Lista<T>::usun_wszystkie()
{
    element <T> *wsk;
    while(glowa != NULL)
    {
        wsk = glowa;
        glowa = glowa->nastepny;
        delete wsk;
    }
    l_elementow = 0;
}

template <typename T>
void Lista<T>::wyswietl()
{
    element <T> *wsk = glowa;
    cout << "LISTA: " << endl;
    cout << "------------------------" << endl;
    while(wsk != NULL)
    {
       cout << wsk->wartosc << " ";
       wsk = wsk->nastepny;
    }
    cout << endl << "------------------------" << endl << endl;
}


int main()
{
    Lista <int> l1; //tworzenie nowej listy

    char option;
	do{
        system("cls");
        l1.wyswietl();
		cout << endl;
		cout << "========= MENU =========" << endl;
		cout << "1) Dodawanie elementu do listy." << endl;
		cout << "   a) Dodaj na poczatek." << endl;
		cout << "   b) Dodawanie w wybrane miejsce." << endl;
		cout << "2) Usuwanie elementu listy." << endl;
        cout << "   c) Usuwanie pojedynczego." << endl;
        cout << "   d) Usuwanie wszystkich elementow." << endl;
        cout << "0) Wyjscie" << endl;
        cout << "========================" << endl;
		cout << "Podaj opcje: ";
		option = getche();
		cout << endl;

		switch (option){
		case 'a':
			l1.dodaj_poczatek();
			break;

		case 'b':
		    l1.dodaj_wybrany();
			break;

		case 'c':
		    l1.usun_wybrany();
			break;

        case 'd':
		    l1.usun_wszystkie();
			break;
		}

	} while (option != '0');


    return 0;
}
