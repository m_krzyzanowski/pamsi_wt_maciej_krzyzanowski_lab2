#include <iostream>
#include <list>
#include <windows.h>
#include <conio.h>
#include "lista.h"

void Lista::dodaj_element_poczatek()
{
    int liczba;
    cout << "Podaj liczbe: ";
    cin >> liczba;
    lista.push_front(liczba);
}

void Lista::dodaj_element_koniec()
{
    int liczba;
    cout << "Podaj liczbe: ";
    cin >> liczba;
    lista.push_back(liczba);
}

void Lista::wyswietl()
{
    cout << "LISTA: " << endl;
    cout << "------------------------" << endl;
    // begin() Zwraca iterator wskazujący na pierwszy element listy.
    for(i=lista.begin(); i!= lista.end(); ++i)
       cout<<*i<<" ";
    cout << endl << "------------------------" << endl << endl;

}

void Lista::menu_lista()
{
    char option;
	do{
        system("cls");
        wyswietl();
		cout << "==== MENU KOLEJKA ===" << endl;
		cout << "1) Dodawanie elementu do listy." << endl;
		cout << "   a) Dodaj element na poczatek listy." << endl;
		cout << "   b) Dodaj element na koniec listy." << endl;
		cout << "2) Usuwanie. " << endl;
		cout << "   c) Usun z poczatku listy." << endl;
		cout << "   d) Usun z konca listy." << endl;
        cout << "   e) Usun wszystkie elementy." << endl;
		cout << "0) Wyjscie." << endl;
		cout << "===================" << endl;
		cout << "Podaj opcje: ";
		option = getche();
		cout << endl;

		switch (option){
		case 'a':
			dodaj_element_poczatek();
			system("cls");
			break;

        case 'b':
            dodaj_element_koniec();
            system("cls");
            break;

		case 'c':
			lista.pop_front();
			break;

        case 'd':
			lista.pop_back();
			break;

        case 'e':
            lista.clear();
            break;

		}
	} while (option != '0');
}
