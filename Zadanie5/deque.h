#include <iostream>
#include <windows.h>
#include <conio.h>
#include <string>
#include <cstdlib>

using namespace std;

template <typename T>
class element
{
public:
	T wartosc;
	element * nastepny;
	element * poprzedni;
	element()
	{
		wartosc = NULL;
		nastepny = NULL;
		poprzedni = NULL;
	}
};

template <typename T>
class Deque
{
private:
    element <T> *glowa;
    element <T> *ogon;
    int l_elementow;

public:
    Deque() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        l_elementow = 0;
    }
    ~Deque() //destruktor
    {
        while(!(isEmpty()))
            usunPierwszy();
    }

    int rozmiar(); //Zwraca ilo�� obiekt�w przechowywanych w deque
    bool isEmpty(); //Zwraca true je�li deque jest pusty
    T pierwszy(); // Zwraca pierwszy obiekt w deque. (Wyrzuca DequeEmptyException je�li deque jest pusty)
    T ostatni(); // Zwraca ostatni obiekt w deque. (Wyrzuca DequeEmptyException je�li deque jest pusty)
    void wstawPierwszy(T); //Dodaje obiekt do poczatku deque�a. IN: element, ktory chcemy wstawic
    bool usunPierwszy(); // Usuwa pierwszy obiekt z deque. (Wyrzuca DequeEmptyException je�li deque jest pusty oraz zwraca false)
    void wstawOstatni(T); //Dodaje obiekt na ko�cu deque�a. IN: element, ktory chcemy wstawic
    bool usunOstatni(); //Usuwa ostatni obiekt z deque.. (Wyrzuca DequeEmptyException je�li deque jest pusty raz zwraca false)
    void wyswietl(); //wyswietla kolejke
    void menu_deque(); //funkcja realizuje funkcjonalnosc menu
};


template <typename T>
int Deque<T>::rozmiar()
{
    return l_elementow;
}

template <typename T>
bool Deque<T>::isEmpty()
{
    if(l_elementow==0) return true;
    else return false;
}

template <typename T>
T Deque<T>::pierwszy()
{
    if(l_elementow==0) cout << "DequeEmptyException";
    else return glowa->wartosc;
}

template <typename T>
T Deque<T>::ostatni()
{
    if(l_elementow==0) cout << "DequeEmptyException";
    else return ogon->wartosc;
}

template <typename T>
void Deque<T>::wstawPierwszy(T elem)
{
	element <T> *nowy = new element <T>;
    nowy->wartosc = elem; //ustawia wartosc kontenera
    nowy->nastepny = NULL; //ustawia wskaznik na kolejny el.
    nowy->poprzedni = NULL;
    if(l_elementow==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
        nowy->nastepny = glowa;
        glowa->poprzedni = nowy;
        glowa = nowy;
    }
    cin.sync();
    l_elementow++;
}

template <typename T>
bool Deque<T>::usunPierwszy()
{
    element <T> *wsk = glowa;
    if(l_elementow==0)
    {
        cout << "DequeEmptyException";
        return false;
    }
    glowa = glowa->nastepny;
    delete wsk;
    l_elementow--;
    return true;
}

template <typename T>
void Deque<T>::wstawOstatni(T elem)
{
	element <T> *nowy = new element <T>;
    nowy->wartosc = elem; //ustawia wartosc kontenera
    nowy->nastepny = NULL; //ustawia wskaznik na kolejny el.
    nowy->poprzedni = NULL;
    if(l_elementow==0)
    {
        glowa=nowy;
        ogon=nowy;
    }
    else
    {
        ogon->nastepny = nowy;
        nowy->poprzedni = ogon;
        ogon=nowy;
    }
    l_elementow++;
}

template <typename T>
bool Deque<T>::usunOstatni()
{
    element <T> *wsk = ogon;
    if(l_elementow==0)
    {
        cout << "DequeEmptyException";
        return false;
    }
    ogon = ogon->poprzedni;
    delete wsk;
    l_elementow--;
    return true;
}

template <typename T>
void Deque<T>::wyswietl()
{
    element <T> *wsk = glowa;
    cout << "KOLEJKA: " << endl;
    cout << "------------------------" << endl;
    while(wsk != NULL)
    {
       cout << wsk->wartosc << " ";
       wsk = wsk->nastepny;
    }
    cout << endl << "------------------------" << endl << endl;
}

template <typename T>
void Deque<T>::menu_deque()
{
    char option;
        do{
            Sleep(500);
            system("cls");
            Deque::wyswietl();
            cout << "======== MENU ========" << endl;
            cout << "1) rozmiar() - zwraca ilo�� obiekt�w przechowywanych w deque" << endl;
            cout << "2) isEmpty() - zwraca true je�li deque jest pusty" << endl;
            cout << "3) pierwszy() - zwraca pierwszy obiekt w deque." << endl;
            cout << "4) ostatni() - zwraca ostatni obiekt w deque." << endl;
            cout << "5) wstawPierwszy() - dodaje obiekt do poczatku deque�a." << endl;
            cout << "6) usunPierwszy() - usuwa pierwszy obiekt z deque." << endl;
            cout << "7) wstawOstatni() - dodaje obiekt na ko�cu deque�a." << endl;
            cout << "8) usunOstatni() - usuwa ostatni obiekt z deque." << endl;
            cout << "0) Wyjdz." << endl;
            cout << "======================" << endl;
            cout << "Podaj opcje: ";
            option = getche();
            cout << endl;

            switch(option)
            {
            case '1':
                cout << endl << Deque::rozmiar();
                break;

            case '2':
                if(Deque::isEmpty()) cout << "true";
                    else cout << "false";
                break;

            case '3':
                cout << endl << Deque::pierwszy();
                break;

            case '4':
                cout << endl << Deque::ostatni();
                break;

            case '5':
                T elem;
                cout << "PODAJ: ";
                while(!(cin >> elem))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
                {
                    cerr << "(Niepoprawna wartosc.)" << endl;
                    cin.clear(); //kasowanie flagi b��du strumienia
                    cin.sync(); //kasowanie zb�dnych znak�w z bufora
                }
                Deque::wstawPierwszy(elem);
                break;

            case '6':
                Deque::usunPierwszy();
                break;

            case '7':
                T elemn;
                cout << "PODAJ: ";
                while(!(cin >> elemn))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
                {
                    cerr << "(Niepoprawna wartosc.)" << endl;
                    cin.clear(); //kasowanie flagi b��du strumienia
                    cin.sync(); //kasowanie zb�dnych znak�w z bufora
                }
                Deque::wstawOstatni(elemn);
                break;

            case '8':
                Deque::usunOstatni();
                break;
            }

            } while (option != '0');

}
