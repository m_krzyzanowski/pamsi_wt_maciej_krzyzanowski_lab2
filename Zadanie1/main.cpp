#include <iostream>
#include <string>
#include <cstdlib>
#include <vector>

using namespace std;

vector <string> palList; //przechowuje liste palindromow

struct wezel
{
    string element; //wartosc elementu
    wezel *nastepny; //wskaznik na kolejny elemenet
    wezel()
    {
        nastepny = NULL; //konstruktor wskaznika
    }
};

struct lista
{
    wezel *pierwszy;  //wskaznik na pierwszy el. listy
    void Permutacje(string,int); //funkja wypelnionia liste permutacjami danego stringu
    void wyswietlListe(); //funkcja wyswietla zawartosc listy
    bool jestPal(string); //funkja sprawdza czy podany dtring jest palindromem
    void wypelnij_palList(); //wypelnia palList znalezionymi palindromami
    lista()
    {
        pierwszy = NULL;  //konstruktor wskaznika
    }
};

void lista::Permutacje(string testStr, int p)
{
    int rozmiar = testStr.length()-1;
	if (p == rozmiar)
    {
        wezel *nowy = new wezel; //tworzy nowy kontener
        nowy->element = testStr; //ustawia wartosc kontenera
        nowy->nastepny = pierwszy; //ustawia wskaznik na kolejny el.
        pierwszy = nowy; //ustawia nowy poczatek listy
	}
    else
    {
        for (int i=p; i<=rozmiar; i++)
        {
         swap(testStr[i],testStr[p]);    // zamienia miejscami i-ty element z p-tym
         Permutacje(testStr,p+1);        // wywolanie funkcji dla zmienionego znaku
         swap(testStr[i],testStr[p]);    // przywracanie znakowi poprzeniego stanu
		}
    }
}

void lista::wyswietlListe()
{
    wezel *wsk = pierwszy; //wskaznik na pierwszy element listy
    //petla wyswietla zawartosc listy dopoki nie na potka konca listy (NULL)
    while(wsk)
    {
        cout << wsk->element << " ";
        wsk = wsk->nastepny;
    }
}

bool lista::jestPal(string testStr)
{
    int dlugosc = testStr.length();
    if (dlugosc==0 || dlugosc==1) return true; //zdarzenie elementarne
    //fukcja sprawda czy string ma taki sam pierwszy i ostani znak
    if (testStr.front() == testStr.back())
    {
        testStr.erase(testStr.begin()); //usuniecie pierwszego znaku
        testStr.pop_back(); //usuniecie ostatniego znaku
        return jestPal(testStr);
    }
    else return false;
}

void lista::wypelnij_palList()
{
    wezel *wsk;
    for(wsk = pierwszy; wsk != NULL; wsk=wsk->nastepny)
    {
        if(jestPal(wsk->element)) //warunek sprawdza czy dany string jest palindromem, jesli jest wstawia go do palList
        {
            palList.push_back(wsk->element);
        }
    }
}

void usunDup()
{
    //petle przechodza przez przez kolejne elementy listy, gdy znajda duplikat usuwaja go
    for( int i = 0; i < palList.size(); i++ )
    {
        for( int j = 0; j < palList.size(); j++ )
        {
            if(palList[i] == palList[j])
                palList.erase(palList.begin()+i);
        }
    }
}

void wyswietlTablice()
{
    for(int i=0; i<palList.size(); i++)
        cout << palList[i] << " ";
}

int main()
{
    lista *permutacje = new lista; //alokacja listy przechowujacej permutacje

    string slowo;
    cout << "Podaj slowo: ";
    cin >> slowo;

    permutacje->Permutacje(slowo,0);
    cout << "-----------Permutacje-----------" << endl;
    permutacje->wyswietlListe();
    cout << endl << "-----------------------------" << endl << endl;

    permutacje->wypelnij_palList();
    cout << "-----------palList-----------" << endl;
    wyswietlTablice();
    cout << endl << "-----------------------------" << endl << endl;

    usunDup();
    cout << "------usunDup -> palList-----" << endl;
    wyswietlTablice();
    cout << endl << "-----------------------------" << endl << endl;

    return 0;
}





