#include <iostream>
#include <conio.h>
#include <windows.h>

using namespace std;

template <typename T>
struct element
{
    T wartosc;
	element *nastepny;
	element()           //konstruktor elementu
	{
		wartosc = NULL;
		nastepny = NULL;
	}
};

template <typename T>
struct Kolejka
{
    element <T> *glowa;
    element <T> *ogon;
    void wyswietl(); //wyswietla zawartosc kolejki
    void dodaj_element(); //dodaje element na koniec kolejki
    bool usun_element(); //usuwa element z poczatku kolejki (Zwraca TRUE jesli sie udalo, zwrata FALSE i wyswietla odpowiedni komunkat jesli sie nie udalo)
    void usun_wszystkie(); //usuwa wszystkie elementy kolejki
    void menu_kolejka(); //wyswietla menu kolejki
    int l_elementow;

    Kolejka() //konstruktor
    {
        glowa = NULL;
        ogon = NULL;
        l_elementow = 0;
    }

    ~Kolejka() //destruktor
    {
        element <T> *wsk;
        while(glowa)
        {
            wsk = glowa;
            glowa = glowa -> nastepny;
            delete wsk;
        }
    }

};

template <typename T>
void Kolejka<T>::wyswietl()
{
    element <T> *wsk = glowa;
    cout << "KOLEJKA: " << endl;
    cout << "------------------------" << endl;
    while(wsk != NULL)
    {
       cout << wsk->wartosc << " ";
       wsk = wsk->nastepny;
    }
    cout << endl << "------------------------" << endl << endl;
}

template <typename T>
void Kolejka<T>::dodaj_element()
{
    T elem;
    cout << "PODAJ: ";
    while(!(cin >> elem))  //dopoki strumien wejsciowy jest w stanie bledu wykonywane sa instrukcje:
    {
        cerr << "(Niepoprawna wartosc.)" << endl;
        cin.clear(); //kasowanie flagi b��du strumienia
        cin.sync(); //kasowanie zb�dnych znak�w z bufora
    }
	element <T> *nowy = new element <T>;
    nowy->wartosc = elem;
    nowy->nastepny = NULL;
    if(l_elementow==0)
    {
        glowa = nowy;
        ogon = nowy;
    }
    else
    {
		ogon->nastepny = nowy;
		ogon = nowy;
    }
    cin.sync();
    l_elementow++;
}

template <typename T>
bool Kolejka<T>::usun_element()
{
    element <T> *wsk = glowa;
    if(!(wsk))
    {
        cout << "Kolejka jest pusta.";
        Sleep(1000);
        return false;
    }
    glowa = glowa->nastepny;
    delete wsk;
    l_elementow--;
    return true;
}

template <typename T>
void Kolejka<T>::usun_wszystkie()
{
    element <T> *wsk;
    while(glowa != NULL)
    {
        wsk = glowa;
        glowa = glowa->nastepny;
        delete wsk;
    }
    l_elementow = 0;
}



int main()
{
        Kolejka <int> k1;


        char option;
        do{
            system("cls");
            k1.wyswietl();
            cout << "==== MENU KOLEJKA ===" << endl;
            cout << "1) Dodawanie elementu do kolejki." << endl;
            cout << "2) Usuwanie pojedynczego elementu z kolejki." << endl;
            cout << "3) Usuwanie wszystkich elementow z kolejki." << endl;
            cout << "0) Wyjdz." << endl;
            cout << "===================" << endl;
            cout << "Podaj opcje: ";
            option = getche();
            cout << endl;

            switch(option)
            {
            case '1':
                k1.dodaj_element();
                break;

            case '2':
                k1.usun_element();
                break;

            case '3':
                k1.usun_wszystkie();
                break;
            }

            } while (option != '0');

    return 0;
}
