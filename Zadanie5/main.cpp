#include <iostream>
#include <string>
#include "deque.h"

using namespace std;

template <typename T>
bool jestPal(T & testStr)
{
    int dlugosc = testStr.rozmiar();
    if (dlugosc==0 || dlugosc==1) return true; //zdarzenie elementarne
    //fukcja sprawda czy slowo ma taki sam pierwszy i ostani znak
    if (testStr.pierwszy() == testStr.ostatni())
    {
        testStr.usunPierwszy(); //usuniecie pierwszego znaku
        testStr.usunOstatni(); //usuniecie ostatniego znaku
        return jestPal(testStr);
    }
    else return false;
}

int main()
{
    Deque <int> q1;
    q1.wstawPierwszy(6);

    //sprawdzeni funkcjonalnosci kolejki
//    Deque <int> q1;
//    q1.menu_deque();

    return 0;
}
