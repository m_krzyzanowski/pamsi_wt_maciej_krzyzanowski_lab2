#include <iostream>
#include <list>

using namespace std;

class Lista
{
public:
    list <int> lista; // alokacja listy przechowujacej l. calkowice
    list<int>::iterator i;

    void dodaj_element_poczatek();
    void dodaj_element_koniec();
    void wyswietl();
    void menu_lista();
};
